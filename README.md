# heat-conduction-test

[![pipeline status](https://gitlab.com/kavalchuk.d.v/heat-conduction-test/badges/master/pipeline.svg)](https://gitlab.com/kavalchuk.d.v/heat-conduction-test/-/commits/master)

## Description
Heat conduction equation solving for 3rd boundary value problem. Program provides possibility to calculate array of temperatures after some time interval from initial array of temperatures. Initial array of temperatures is generated automatically by sin function. Max value of initial temperatures is user-defined.

## Using
./heatconduction [filename] - calculation using file [filename] with all parameters. File example provided in root folder of repository. You are able omit some parameters. Default parameter will be used for it.
./heatconduction - calculation using default parameters
