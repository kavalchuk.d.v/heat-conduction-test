#include "temperature_calculator.hpp"
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>
#include <stdexcept>

#ifdef NDEBUG
static constexpr bool g_isDebug{ false };
#else
static constexpr bool g_isDebug{ true };
#endif

namespace Solver
{
std::unique_ptr<ITemperatureCalculator>
ITemperatureCalculator::createCalculatorExplicitDifference(
    const ITemperatureCalculator::InitialParameters& initialParameters)
{
    return std::make_unique<TemperatureCalculator>(initialParameters);
}

template <typename T>
static bool isSame(T value1, T value2);

using calculationType = TemperatureCalculator::calculationType;

template <typename T>
static void checkBounds(const std::string& parameterName, T parameter,
                        T lowerBound,
                        T upperBound = std::numeric_limits<T>::max());

static void checkAllIntialParametesBounds(
    const TemperatureCalculator::InitialParameters& initializeParameters);

TemperatureCalculator::TemperatureCalculator(
    const InitialParameters& initialParameters)
{
    initialize(initialParameters);
}

void TemperatureCalculator::initialize(
    const InitialParameters& initialParameters)
{
    checkAllIntialParametesBounds(initialParameters);

    checkAndInitRodCoordinates(initialParameters.rodBegin,
                               initialParameters.rodEnd);

    m_numberOfParticles = initialParameters.numberOfParticles;
    m_numberOfNodes     = m_numberOfParticles + 1;

    m_stepCoordinate = m_rodLength / m_numberOfParticles;

    m_a2 = getA2(initialParameters.heatConductionCoef,
                 initialParameters.heatCapacity, initialParameters.density);

    m_k1Left  = getK1(initialParameters.heatConductionCoef, m_stepCoordinate,
                     initialParameters.heatExchangeCoefLeft,
                     initialParameters.environementTemperatureLeft);
    m_k1Right = getK1(initialParameters.heatConductionCoef, m_stepCoordinate,
                      initialParameters.heatExchangeCoefRight,
                      initialParameters.environementTemperatureRight);
    m_k2Left  = getK2(initialParameters.heatConductionCoef, m_stepCoordinate,
                     initialParameters.heatExchangeCoefLeft);
    m_k2Right = getK2(initialParameters.heatConductionCoef, m_stepCoordinate,
                      initialParameters.heatExchangeCoefRight);
}

void TemperatureCalculator::checkAndInitRodCoordinates(
    const calculationType initialRodBegin, const calculationType initialRodEnd)
{
    if (initialRodEnd > initialRodBegin)
    {
        m_rodBeginCoordinate = initialRodBegin;
        m_rodEndCoordinate   = initialRodEnd;
    }
    else
    {
        m_rodBeginCoordinate = initialRodEnd;
        m_rodEndCoordinate   = initialRodBegin;
    }

    m_rodLength = m_rodEndCoordinate - m_rodBeginCoordinate;

    checkBounds("Lenght of rod", m_rodLength, 0.0);
}

std::tuple<calculationType, calculationType, calculationType>
TemperatureCalculator::calculateStepTimeSigmaNumberIntervals(
    calculationType timeIntervalToUpdate)
{
    const auto stepCoordinate2 = m_stepCoordinate * m_stepCoordinate;

    const auto desirableSigma = maxSigmaBoundForConvergentCondition /
                                m_marginForSigmaConvergentCondition;

    const auto stepTimeFloat = desirableSigma * stepCoordinate2 / m_a2;

    const auto numberOfIntervals =
        std::ceil(timeIntervalToUpdate / stepTimeFloat);

    const auto stepTime = timeIntervalToUpdate / numberOfIntervals;

    const auto sigma = m_a2 * stepTime / stepCoordinate2;

    return { stepTime, sigma, numberOfIntervals };
}

calculationType TemperatureCalculator::getMarginForConvergentCondition()
    const noexcept
{
    return m_marginForSigmaConvergentCondition;
}

void TemperatureCalculator::setMarginForConvergentCondition(
    calculationType in_margin) noexcept
{
    if (in_margin < 1.0)
    {
        m_marginForSigmaConvergentCondition = 1.0;
        std::cerr
            << "Cannot set margin: must bigger or equal than 1. Minimal value "
            << m_marginForSigmaConvergentCondition << " has been used."
            << std::endl;
    }
    m_marginForSigmaConvergentCondition = in_margin;
}

void TemperatureCalculator::updateTemperatureForTime(
    calculationType               timeIntervalToUpdate,
    std::vector<calculationType>& r_temperatures)
{
    const auto [stepTime, sigma, numberOfTimeIntervals] =
        calculateStepTimeSigmaNumberIntervals(timeIntervalToUpdate);

    for (auto currentIteration = 0; currentIteration < numberOfTimeIntervals;
         ++currentIteration)
    {
        calculateNextTemperatures(r_temperatures, sigma);
        if (g_isDebug)
        {
            printIterationValues(std::clog, r_temperatures,
                                 m_rodBeginCoordinate, m_stepCoordinate,
                                 currentIteration * stepTime);
        }
    }
}

void TemperatureCalculator::calculateNextTemperatures(
    std::vector<calculationType>& currentIterationTemperatures,
    const calculationType         sigma)
{
    const auto initialPreBoundTemperatureLeft = currentIterationTemperatures[1];
    const auto initialPreBoundTemperatureRight =
        currentIterationTemperatures[m_numberOfNodes - 2];
    calculateNextTemperaturesGeneral(currentIterationTemperatures, sigma);
    calculateNextTemperaturesBounds(currentIterationTemperatures,
                                    initialPreBoundTemperatureLeft,
                                    initialPreBoundTemperatureRight);
}

void TemperatureCalculator::calculateNextTemperaturesGeneral(
    std::vector<calculationType>& currentIterationTemperatures,
    const calculationType         sigma)
{
    auto lastValueOfPreviousParticle = currentIterationTemperatures[0];
    auto calcNextTemperatureGeneral  = [sigma, &lastValueOfPreviousParticle,
                                       &currentIterationTemperatures](
                                          size_t currentParticleIndex) {
        const auto currentParticleNewTemperature =
            sigma * (currentIterationTemperatures[currentParticleIndex + 1] +
                     lastValueOfPreviousParticle) +
            (1 - 2 * sigma) *
                currentIterationTemperatures[currentParticleIndex];
        lastValueOfPreviousParticle =
            currentIterationTemperatures[currentParticleIndex];
        return currentParticleNewTemperature;
    };
    {
        size_t     currentParticleIndex = 1;
        const auto numberOfParticlesGeneral =
            currentIterationTemperatures.size() - 2;
        std::generate_n(
            ++currentIterationTemperatures.begin(), numberOfParticlesGeneral,
            [&calcNextTemperatureGeneral, &currentParticleIndex]() {
                const auto nextTemperature =
                    calcNextTemperatureGeneral(currentParticleIndex);
                ++currentParticleIndex;
                return nextTemperature;
            });
    }
}

void TemperatureCalculator::calculateNextTemperaturesBounds(
    std::vector<calculationType>& currentIterationTemperatures,
    const calculationType         initialPreBoundTemperaturesLeft,
    const calculationType         initialPreBoundTemperaturesRight)
{
    currentIterationTemperatures.front() =
        m_k1Left + m_k2Left * initialPreBoundTemperaturesLeft;
    currentIterationTemperatures.back() =
        m_k1Left + m_k2Left * initialPreBoundTemperaturesRight;
}

TemperatureCalculator::calculationType TemperatureCalculator::getA2(
    calculationType heatConductionCoef, calculationType heatCapacity,
    calculationType density) noexcept
{
    const auto denominator = (heatCapacity * density);
    if (isSame(denominator, 0.0))
        return 0.0;
    return heatConductionCoef / denominator;
}

calculationType TemperatureCalculator::getK1(
    calculationType heatConductionCoef, calculationType stepX,
    calculationType heatExchangeCoef,
    calculationType environementTemperature) noexcept
{
    const auto denominator = heatConductionCoef + heatExchangeCoef * stepX;
    if (isSame(denominator, 0.0))
        return 0.0;
    return heatExchangeCoef * stepX * environementTemperature / denominator;
}

TemperatureCalculator::calculationType TemperatureCalculator::getK2(
    calculationType heatConductionCoef, calculationType stepX,
    calculationType heatExchangeCoef) noexcept
{
    const auto denominator = heatConductionCoef + heatExchangeCoef * stepX;
    if (isSame(denominator, 0.0))
        return 0.0;
    return heatConductionCoef / denominator;
}

template <typename T>
bool isSame(T value1, T value2)
{
    if (std::abs(value1 - value2) < std::numeric_limits<T>::epsilon())
        return true;
    return false;
}

template <typename T>
static void checkBounds(const std::string& parameterName, T parameter,
                        T lowerBound, T upperBound)
{
    if (parameter < lowerBound)
    {
        std::string errorStr = parameterName + " coeficient must be within " +
                               std::to_string(lowerBound) + "and" +
                               std::to_string(upperBound) +
                               ". Received: " + std::to_string(parameter);
        // std::cerr << errorStr << std::endl;
        throw std::invalid_argument(errorStr);
    }
    else if (parameter > upperBound)
    {
        std::string errorStr = parameterName + " coeficient must be within " +
                               std::to_string(lowerBound) + "and" +
                               std::to_string(upperBound) +
                               ". Received: " + std::to_string(parameter);
        // std::cerr << errorStr << std::endl;
        throw std::invalid_argument(errorStr);
    }
}

static void checkAllIntialParametesBounds(
    const TemperatureCalculator::InitialParameters& initializeParameters)
{
    // Every checked values must bigger than zero or celsium absolute zero
    // (-273)
    const auto almostZero = std::numeric_limits<calculationType>::epsilon();
    checkBounds("Number of particles", initializeParameters.numberOfParticles,
                static_cast<uint_least64_t>(1));
    checkBounds("Heat conduction coeficient",
                initializeParameters.heatConductionCoef, almostZero);
    checkBounds("Heat capacity", initializeParameters.heatCapacity, almostZero);

    checkBounds("Density", initializeParameters.density, almostZero);

    checkBounds("Heat exchange coeficient on the left side of rod",
                initializeParameters.heatExchangeCoefLeft, almostZero);
    checkBounds("Heat exchange coeficient on the right side of rod",
                initializeParameters.heatExchangeCoefRight, almostZero);

    checkBounds("Heat exchange coeficient on the left side of rod",
                initializeParameters.environementTemperatureLeft,
                TemperatureCalculator::temperatureAbsoluteZero);
    checkBounds("Heat exchange coeficient on the right side of rod",
                initializeParameters.heatExchangeCoefRight,
                TemperatureCalculator::temperatureAbsoluteZero);
}

bool printIterationValues(
    std::ostream&                       out,
    const std::vector<calculationType>& currentIterationTemperatures,
    calculationType initialCoordiante, calculationType stepCoordinate,
    calculationType currentTime)
{
    int    lineElementsCount{};
    double currentX{ initialCoordiante };
    out << "Iteration time: " << currentTime << " seconds" << '\n';

    out << std::fixed;
    auto printOneElement = [&lineElementsCount, &out, &currentX,
                            stepCoordinate](const calculationType value) {
        if (lineElementsCount == 10)
        {
            out << "\n";
            lineElementsCount = 0;
        }
        out << "X: " << std::setprecision(2) << std::setw(4) << currentX << "; "
            << "T: " << std::setprecision(2) << std::setw(5) << value << " | ";
        currentX += stepCoordinate;
        ++lineElementsCount;
    };

    std::for_each(currentIterationTemperatures.begin(),
                  currentIterationTemperatures.end(), printOneElement);
    return static_cast<bool>(out);
}
} // namespace Solver
