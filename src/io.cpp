#include "io.hpp"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <unordered_map>
#include <utility>
#ifdef LAST_FEATURES
#include <numbers>
#else
#define _USE_MATH_DEFINES
#include <math.h>
#endif

using namespace Solver;
using calculationType   = ITemperatureCalculator::calculationType;
using InitialParameters = ITemperatureCalculator::InitialParameters;

static ProgramInitialParameters getDefaultInitialParameters();

using namespace std::string_view_literals;
class FileParser
{
public:
    enum class ParameterType
    {
        rodBegin,
        rodEnd,
        timeToPredict,
        density,
        heatCapacity,
        heatConductionCoef,
        numberOfParticles,
        heatExchangeCoefLeft,
        heatExchangeCoefRight,
        environementTemperatureLeft,
        environementTemperatureRight,
        maxInitialTemperature,
        convergentMargin,
        errorParam
    };

    static const std::unordered_map<std::string_view, ParameterType>
        parametersMap;

    static bool getInitialParametersFromFile(
        std::string_view          filePath,
        ProgramInitialParameters& r_initialParameters);

private:
    static std::pair<ParameterType, calculationType> parseOneLine(
        std::string_view line);
    static ParameterType getDataTypeFromStr(std::string_view parameterName);
    static bool getDoubleFromStr(std::string_view str, double& r_number);
};

ProgramInitialParameters getInitialParameters(std::string_view filePath)
{
    ProgramInitialParameters initialParameters = getDefaultInitialParameters();
    if (filePath.empty())
    {
        return initialParameters;
    }

    if (!FileParser::getInitialParametersFromFile(filePath, initialParameters))
    {
        std::cerr << "Unable to get initial parameters from file. Default "
                     "parameters is used."
                  << std::endl;
        return getDefaultInitialParameters();
    }
    return initialParameters;
}

std::vector<calculationType> generateNodesInitialTemperature(
    calculationType maxInitialTemperature, uint_least32_t numberOfNodes)
{
#ifdef LAST_FEATURES
    const auto pi = std::numbers::pi_v<calculationType>;
#else
    const auto pi = M_PI;
#endif
    std::vector<double> initialTemperatures{};

    auto temperatureSinGenerator = [pi, maxInitialTemperature,
                                    numberOfNodes]() {
        static auto       currentNode = 0;
        static const auto constKoef   = pi / (numberOfNodes - 1);
        const auto        angle       = constKoef * currentNode;
        ++currentNode;
        return maxInitialTemperature * std::sin(angle);
    };

    std::generate_n(std::back_inserter(initialTemperatures), numberOfNodes,
                    temperatureSinGenerator);

    return initialTemperatures;
}

bool printTemperatures(std::ostream&                       out,
                       const std::vector<calculationType>& resultTemperatures,
                       InitialParameters                   initialParameters,
                       calculationType timeToUpdate, std::string_view groupName)
{
    std::cout << groupName << '\n';
    const auto stepCoordinate =
        (initialParameters.rodEnd - initialParameters.rodBegin) /
        initialParameters.numberOfParticles;
    auto result = printIterationValues(out, resultTemperatures,
                                       initialParameters.rodBegin,
                                       stepCoordinate, timeToUpdate);
    std::cout << std::endl;
    return result;
}

static ProgramInitialParameters getDefaultInitialParameters()
{
    // Parameters by specification
    static const auto defaultRodBegin                     = 0.0;
    static const auto defaultRodEnd                       = 1.0;
    static const auto defaultTimeToPredict                = 1.0;
    static const auto defaultDensity                      = 2.69;
    static const auto defaultHeatCapacity                 = 903.7;
    static const auto defaultHeatConductionCoef           = 237.0;
    static const auto defaultNumberOfParticles            = 99;
    static const auto defaultHeatExchangeCoefLeft         = 50.0;
    static const auto defaultHeatExchangeCoefRight        = 50.0;
    static const auto defaultEnvironementTemperatureLeft  = 35.0;
    static const auto defaultEnvironementTemperatureRight = 35.0;

    // Parameter used to generate initial temperature's vector
    static const auto defaultMaxInitialTemperature = 100.0;

    // Parameter used to generate initial temperature's vector
    static const auto defaultMarginForSigmaConvergentCondition = 2.0;

    ITemperatureCalculator::InitialParameters initialParameters{
        defaultRodBegin,
        defaultRodEnd,
        defaultDensity,
        defaultHeatCapacity,
        defaultHeatConductionCoef,
        defaultNumberOfParticles,
        defaultHeatExchangeCoefLeft,
        defaultHeatExchangeCoefRight,
        defaultEnvironementTemperatureLeft,
        defaultEnvironementTemperatureRight
    };
    return { initialParameters, defaultTimeToPredict,
             defaultMaxInitialTemperature,
             defaultMarginForSigmaConvergentCondition };
}

static bool openInputFile(std::string_view fileName, std::ifstream& r_file)
{
    r_file.open(fileName.data(), std::ios::binary);
    if (!static_cast<bool>(r_file))
    {
        std::cerr << "Cannot open input file " << fileName << std::endl;
        return false;
    }
    return true;
}

static std::vector<std::string_view> splitLineToWords(std::string_view line,
                                                      const char delimeter)
{
    using namespace std;
    if (line.empty())
    {
        return {};
    }

    size_t countSpaces = std::count(line.begin(), line.end(), delimeter);

    std::vector<size_t> startNextWord;
    startNextWord.reserve(countSpaces + 1);
    startNextWord.push_back(0); // first index

    size_t i = 0;

    auto copySpaceIndex = [&startNextWord, &i, delimeter](char value) {
        ++i;
        if (value == delimeter)
        {
            startNextWord.push_back(i);
        }
    };

    for_each(begin(line), end(line), copySpaceIndex);

    vector<string_view> result;
    result.reserve(countSpaces + 1);

    transform((startNextWord.begin()), --(startNextWord.end()),
              ++(startNextWord.begin()), back_inserter(result),
              [&line](size_t first_index, size_t last_index) {
                  return line.substr(first_index, --last_index - first_index);
              });

    auto last_word = line.substr(startNextWord.back());
    result.push_back(last_word);

    return result;
}

const std::unordered_map<std::string_view, FileParser::ParameterType>
    FileParser::parametersMap{
        { "rod begin"sv, ParameterType::rodBegin },
        { "rod end"sv, ParameterType::rodEnd },
        { "time to predict"sv, ParameterType::timeToPredict },
        { "rod density"sv, ParameterType::density },
        { "rod heat capacity"sv, ParameterType::heatCapacity },
        { "rod heat conduction"sv, ParameterType::heatConductionCoef },
        { "rod number of particles"sv, ParameterType::numberOfParticles },
        { "heat exchange coeficient left"sv,
          ParameterType::heatExchangeCoefLeft },
        { "heat exchange coeficient right"sv,
          ParameterType::heatExchangeCoefRight },
        { "environement temperature left"sv,
          ParameterType::environementTemperatureLeft },
        { "environement temperature right"sv,
          ParameterType::environementTemperatureRight },
        { "maximal initial temperature"sv,
          ParameterType::maxInitialTemperature },
        { "equations solution convergent margin"sv,
          ParameterType::convergentMargin }
    };

/// Reading initial parameters from file. Parameters that dont appear in file
/// will be used by default. If file contains any error reading will be closed
/// and will be used all default parameters
bool FileParser::getInitialParametersFromFile(
    std::string_view filePath, ProgramInitialParameters& r_initialParameters)
{

    std::ifstream inputFile;
    if (!openInputFile(filePath, inputFile))
    {
        return false;
    }

    std::string line;
    while (std::getline(inputFile, line, '\n'))
    {
        // comments omiting
        if (line[0] == '#')
        {
            continue;
        }
        const auto [parameterType, value] = parseOneLine(line);
        switch (parameterType)
        {
            case ParameterType::rodBegin:
                r_initialParameters.solverInitialParameters.rodBegin = value;
                break;
            case ParameterType::rodEnd:
                r_initialParameters.solverInitialParameters.rodEnd = value;
                break;
            case ParameterType::timeToPredict:
                r_initialParameters.timeToPredict = value;
                break;
            case ParameterType::density:
                r_initialParameters.solverInitialParameters.density = value;
                break;
            case ParameterType::heatCapacity:
                r_initialParameters.solverInitialParameters.heatCapacity =
                    value;
                break;
            case ParameterType::heatConductionCoef:
                r_initialParameters.solverInitialParameters.heatConductionCoef =
                    value;
                break;
            case ParameterType::numberOfParticles:
                r_initialParameters.solverInitialParameters.numberOfParticles =
                    static_cast<decltype(InitialParameters::numberOfParticles)>(
                        value);
                break;
            case ParameterType::heatExchangeCoefLeft:
                r_initialParameters.solverInitialParameters
                    .heatExchangeCoefLeft = value;
                break;
            case ParameterType::heatExchangeCoefRight:
                r_initialParameters.solverInitialParameters
                    .heatExchangeCoefRight = value;
                break;
            case ParameterType::environementTemperatureLeft:
                r_initialParameters.solverInitialParameters
                    .environementTemperatureLeft = value;
                break;
            case ParameterType::environementTemperatureRight:
                r_initialParameters.solverInitialParameters
                    .environementTemperatureRight = value;
                break;
            case ParameterType::maxInitialTemperature:
                r_initialParameters.maxInitialTemperature = value;
                break;
            case ParameterType::convergentMargin:
                r_initialParameters.convergentMargin = value;
                break;
            default:
                std::cerr << "Unable to read parameter." << std::endl;
                return false;
        }
    }
    return true;
}

std::pair<FileParser::ParameterType, calculationType> FileParser::parseOneLine(
    std::string_view line)
{
    const auto wordsTypeAndData = splitLineToWords(line, ':');
    const auto parameterType    = getDataTypeFromStr(wordsTypeAndData[0]);

    const std::pair<FileParser::ParameterType, calculationType> errorValues{
        ParameterType::errorParam, 0.0
    };

    if (parameterType == ParameterType::errorParam)
    {
        return errorValues;
    }

    double tempValue;

    if (!getDoubleFromStr(wordsTypeAndData[1], tempValue))
    {
        return errorValues;
    }

    const auto value =
        static_cast<ITemperatureCalculator::calculationType>(tempValue);
    return { parameterType, value };
}

FileParser::ParameterType FileParser::getDataTypeFromStr(
    std::string_view parameterName)
{
    const auto typeIterator = parametersMap.find(parameterName);
    if (typeIterator == parametersMap.end())
    {
        std::cerr << "Cannot find type of parameter. Received name: "
                  << parameterName << std::endl;
        return ParameterType::errorParam;
    }
    return typeIterator->second;
}

bool FileParser::getDoubleFromStr(std::string_view str, double& r_number)
{
    const auto stringData = static_cast<std::string>(str);
    try
    {
        const auto value = std::stod(stringData);
        r_number         = value;
    }
    catch (std::exception& ex)
    {
        std::cerr << "Cannot parse double: " << ex.what() << std::endl;
    }

    return true;
}
