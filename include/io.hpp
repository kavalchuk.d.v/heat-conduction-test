#pragma once
#include <itemperature_calculator.hpp>

#include <iosfwd>
#include <string_view>
#include <vector>

struct ProgramInitialParameters
{
    Solver::ITemperatureCalculator::InitialParameters solverInitialParameters{};
    Solver::ITemperatureCalculator::calculationType   timeToPredict{};
    Solver::ITemperatureCalculator::calculationType   maxInitialTemperature{};
    Solver::ITemperatureCalculator::calculationType   convergentMargin{};
};

/// get structure contained initial parameters. If path is empyty default
/// parameters is used
ProgramInitialParameters getInitialParameters(std::string_view filePath);

std::vector<Solver::ITemperatureCalculator::calculationType>
generateNodesInitialTemperature(
    Solver::ITemperatureCalculator::calculationType maxInitialTemperature,
    uint_least32_t                                  numberOfNodes);

bool printTemperatures(
    std::ostream& out,
    const std::vector<Solver::ITemperatureCalculator::calculationType>&
                                                      resultTemperatures,
    Solver::ITemperatureCalculator::InitialParameters initialParameters,
    Solver::ITemperatureCalculator::calculationType   timeToUpdate,
    std::string_view                                  groupName);
