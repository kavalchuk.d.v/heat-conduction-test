#pragma once
#include <cstdint>
#include <iosfwd>
#include <memory>
#include <vector>
#ifndef T_DECLSPEC
#define T_DECLSPEC
#endif

namespace Solver
{
/// interface for temperature calculator. Use to hide implementation and to have
/// possibilities to add calculators with different type of solver
struct T_DECLSPEC ITemperatureCalculator
{
    using calculationType = double;
    struct InitialParameters
    {
        /// coordinate of begin of the rod
        calculationType rodBegin{};
        /// coordinate of begin of the rod
        calculationType rodEnd{};
        /// density of rod material
        calculationType density{};
        /// heat capacity of rod material
        calculationType heatCapacity{};
        /// heat conduction coeficient of rod material
        calculationType heatConductionCoef{};
        /// number of particles of rod for calculation model
        uint_least64_t numberOfParticles{};
        /// heat exchange coeficient on the left bound of rod (coordinate
        /// smaller)
        calculationType heatExchangeCoefLeft{};
        /// constant environement temperature on the right bound of rod
        /// (coordinate larger)
        calculationType heatExchangeCoefRight{};
        /// constant environement temperature degrees Celsius on the left bound
        /// of rod (coordinate smaller)
        calculationType environementTemperatureLeft{};
        /// constant environement temperature degrees Celsius on the right bound
        /// of rod (coordinate larger)
        calculationType environementTemperatureRight{};
    };
    virtual ~ITemperatureCalculator() {}

    virtual calculationType getMarginForConvergentCondition()
        const noexcept = 0;
    virtual void setMarginForConvergentCondition(
        calculationType in_margin) noexcept = 0;

    /// create calculator that use explicit difference scheme to solve heat
    /// conduction equations
    virtual void updateTemperatureForTime(
        calculationType               timeIntervalToUpdate,
        std::vector<calculationType>& r_temperatures) = 0;
    static std::unique_ptr<ITemperatureCalculator>
    /// create calculator that use explicit difference scheme to solve heat
    /// conduction equations
    createCalculatorExplicitDifference(
        const InitialParameters& initialParameters);
};

bool T_DECLSPEC
printIterationValues(std::ostream& out,
                     const std::vector<ITemperatureCalculator::calculationType>&
                                                             currentIterationTemperatures,
                     ITemperatureCalculator::calculationType initialCoordinate,
                     ITemperatureCalculator::calculationType stepCoordinate,
                     ITemperatureCalculator::calculationType currentTime);
} // namespace Solver
