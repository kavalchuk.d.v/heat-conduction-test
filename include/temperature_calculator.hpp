#pragma once
#include "itemperature_calculator.hpp"

namespace Solver
{
/// Class for calculating result temperatures (hear conduction equations
/// solving) of the ideal rod after defined time interval proceeded for 3-rd
/// boundary problem using explicit differencing scheme
class TemperatureCalculator : public ITemperatureCalculator
{
    // Template for choosing type (double or float) is not used because of
    // "beatiful" error messages provided by compiler for templates
public:
    TemperatureCalculator(const InitialParameters& initialParameters);
    ~TemperatureCalculator() override {}

    calculationType getMarginForConvergentCondition() const noexcept override;
    void            setMarginForConvergentCondition(
                   calculationType in_margin) noexcept override;

    void updateTemperatureForTime(
        calculationType               timeIntervalToUpdate,
        std::vector<calculationType>& r_temperatures) override;

    static constexpr calculationType temperatureAbsoluteZero = -273.15;

private:
    void initialize(const InitialParameters& initialParameters);

    void checkAndInitRodCoordinates(const calculationType initialRodBegin,
                                    const calculationType initialRodEnd);

    double calculateTimeStep();

    void calculateNextTemperatures(
        std::vector<calculationType>& currentIterationTemperatures,
        const calculationType         sigma);

    void calculateNextTemperaturesGeneral(
        std::vector<calculationType>& currentIterationTemperatures,
        const calculationType         sigma);

    void calculateNextTemperaturesBounds(
        std::vector<calculationType>& currentIterationTemperatures,
        const calculationType         initialPreBoundTemperaturesLeft,
        const calculationType         initialPreBoundTemperaturesRight);

    static calculationType getA2(calculationType heatConductionCoef,
                                 calculationType heatCapacity,
                                 calculationType density) noexcept;

    static calculationType getK1(
        calculationType heatConductionCoef, calculationType stepX,
        calculationType heatExchangeCoef,
        calculationType environementTemperature) noexcept;

    static calculationType getK2(calculationType heatConductionCoef,
                                 calculationType stepX,
                                 calculationType heatExchangeCoef) noexcept;

    std::tuple<calculationType, calculationType, calculationType>
    calculateStepTimeSigmaNumberIntervals(calculationType timeIntervalToUpdate);

    calculationType m_rodBeginCoordinate{};
    calculationType m_rodEndCoordinate{};
    calculationType m_rodLength{};
    calculationType m_numberOfParticles{};
    calculationType m_numberOfNodes{};
    calculationType m_stepCoordinate{};
    /// coefficient heatConductionCoef / (heatCapacity * density) in historical
    /// reason called a^2
    calculationType m_a2{};
    calculationType m_k1Left{};
    calculationType m_k1Right{};
    calculationType m_k2Left{};
    calculationType m_k2Right{};

    calculationType m_marginForSigmaConvergentCondition{
        defaultMarginForSigmaConvergentCondition
    };

    static constexpr calculationType maxSigmaBoundForConvergentCondition = 0.5;
    static constexpr calculationType defaultMarginForSigmaConvergentCondition =
        2.0;
};
} // namespace Solver
